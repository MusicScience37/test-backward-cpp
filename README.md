# test-backward-cpp

backward-cpp ライブラリのテスト

## ビルド

[pipenv](https://pipenv.pypa.io/en/latest/)
と
[CMake](https://cmake.org/)
と C++ コンパイラを用意し、
次のようにコマンドを実行していく。

```console
$ pipenv install --dev
$ pipenv shell
$ mkdir build
$ cd build
$ conan install --build missing ..
$ cmake ..
$ cmake --build .
```
