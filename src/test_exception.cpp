#include <sstream>

#include <backward.hpp>

[[noreturn]] void throw_with_backtrace(const std::string& message) {
    std::ostringstream stream;
    stream << message << "\n\n";

    backward::StackTrace st;
    st.load_here();
    backward::Printer p;
    p.object = true;
    p.color_mode = backward::ColorMode::never;
    p.address = true;
    p.print(st, stream);

    throw std::runtime_error(stream.str());
}

void test1() { throw_with_backtrace("Test exception"); }

class Test {
public:
    void test2(const std::string&) { test1(); }
};

int main() {
    Test().test2("abc");
    return 0;
}
