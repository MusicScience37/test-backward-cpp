#include <backward.hpp>

void test1() {
    backward::StackTrace st;
    st.load_here();
    backward::Printer p;
    p.object = true;
    p.color_mode = backward::ColorMode::automatic;
    p.address = true;
    p.print(st, stderr);
}

class Test {
public:
    void test2(const std::string &) { test1(); }
};

int main() {
    Test().test2("abc");
    return 0;
}
